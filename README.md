# Programa de Refuerzo - Ejercicios Resueltos

[**Actividad4.-**](/src/com/prog/Activitat4.java) Escriu un programa en el qual es declaren les variables senceres x e i. Asignales els valors 144 i 999 respectivament.
A continuación, mostra per pantalla el valor de cada variable, la suma, la resta, la divisió i la multiplicació.

[**Actividad5.-**](/src/com/prog/Activitat5.java) Implementa un programa que mostre quant valdran unes deportives amb un preu de 85.00 euros,
si estan rebaixades un 15%.

[**Actividad6.-**](/src/com/prog/Activitat6.java) Implementa un programa que visualitze en pantalla quants diners li donarà el banc a un client després de 6 mesos si posa 2000€
en un compte que li dona el 2,75% d'interès anual. Recorda que al pagar-te els interessos, el banc aplicarà una retenció del 18% per a hisenda.

[**Actividad7.-**](/src/com/prog/Activitat7.java) Escriu un programa conversor de Mb a Kb. Els valor que ha de convertir estaràn guardats en variables
i correspondrá a 40.000 i 36.000.

[**Actividad8.-**](/src/com/prog/Activitat8.java) Escriu un programa que calcule el preu final d'un producte segons la seua base imposable
(preu abans d'impostos) Tenint en compte...

[**Actividad9.-**](/src/com/prog/Activitat9.java)  Escriu un programa per a calcular i mostrar l'àrea (A =2PIr2 + 2PIrh) i el volum (V=PIr 2·h) d'un cilindre.
Els valors del diàmetre i l'alçada se li demanaran a l'usuari.

[**Actividad10.-**](/src/com/prog/Activitat10.java) Escriu un programa que demane a l'usuari els valors dels costats d'un rectangle (ample i alt). A partir d'estes dades,
haurà de calcular i escriure en pantalla les longituds, el perímetre i l'àrea del rectangle.

[**Actividad11.-**](/src/com/prog/Activitat11.java) Una temperatura expressada en graus Celsius (el que coneixem com graus centígrads) pot ser convertida a una temperatura equivalent F
(graus Fahrenheit) d'acord a la següent fórmula: f=(9/5)c+32

[**Actividad12.-**](/src/com/prog/Activitat12.java) Escriu un programa que sol·licite a l'usuari una quantitat de segons i la convertisca en dies, hores, minuts i segons.
Visualitza el resultat per pantalla.

[**Actividad13.-**](/src/com/prog/Activitat13.java) Implementa un programa que demane a l'usuari dos números, els guarde en dos variables i intercanvie els valors.
Ha de mostrar els valors intercanviats.

[**Actividad14.-**](/src/com/prog/Activitat14.java) Volem resoldre expressions del tipus ax+b=c, és a dir, equacions de 1r grau, on x és l'incògnita,
a i b són els coeficients i c és el resultat. Implementa un programa on demanes a l'usuari els valors dels coeficients
i el resultat, i que ens retorne el valor d'x.

[**Actividad15.-**](/src/com/prog/Activitat15.java) Fes un programa que demane a l'usuari un número enter de 3 xifres i mostre la seua descomposició en centenes,
desenes i unitats. Si el número introduït és 258, aleshores mostrarà: 258 = 2^100 + 5^10+8

[**Actividad16.-**](/src/com/prog/Activitat16.java) Dissenya un programa que demane a l'usuari les coordenades dels 2 punts
i calcule la distància que n'hi ha entre ells.                      
               