package com.prog;

/**
 * Escriu un programa conversor de Mb a Kb. Els valor que ha de convertir estaràn guardats en variables
 * i correspondrá a 40.000 i 36.000
 */
public class Activitat7 {

    public static void main(String[] args) {

        int amount1 = 40000;
        int amount2 = 36000;

        int convertedAmount1 = amount1 * 1024;
        int convertedAmount2 = amount2 * 1024;

        System.out.printf(" %d Mb son %d Kb \n", amount1, convertedAmount1);
        System.out.printf(" %d Mb son %d Kb \n", amount1, convertedAmount2);

    }
}
