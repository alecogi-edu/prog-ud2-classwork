package com.prog;

import java.util.Scanner;

/**
 * Una temperatura expressada en graus Celsius (el que coneixem com graus centígrads) pot ser convertida a una temperatura equivalent F
 * (graus Fahrenheit) d'acord a la següent fórmula: f=(9/5)c+32
 */
public class Activitat11 {

    public static void main(String[] args) {

        Scanner scannerKey = new Scanner(System.in);

        System.out.println("Introduce grados Celsiu");
        float celsius = scannerKey.nextFloat();

        float fahrenheit = (9/5f)*celsius+32;
        System.out.printf("El equivalente en grados Farhenheit es %6.2f\n", fahrenheit);

    }
}
