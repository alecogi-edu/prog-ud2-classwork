package com.prog;

/**
 * Implementa un programa que visualitze en pantalla quants diners li donarà el banc a un client després de 6 mesos si posa 2000€
 * en un compte que li dona el 2,75% d'interès anual. Recorda que al pagar-te els interessos, el banc aplicarà una retenció del 18% per a hisenda.
 */
public class Activitat6 {

    public static void main(String[] args) {

        int term = 6;
        int initialAmount = 2000;
        float annualInterest = 0.0275f;
        float withHolding = 0.18f;

        float totalProfit = initialAmount*((annualInterest/12)*term);
        float totalWithHolding = totalProfit*withHolding;

        System.out.println("El dinero inicial es: " + initialAmount);
        System.out.println("Los beneficios generados son: " + totalProfit);
        System.out.println("Las retenciones aplicadas son: " + totalWithHolding);
        System.out.println("Total a Percibir: " + initialAmount + (totalProfit-totalWithHolding));

    }
}
