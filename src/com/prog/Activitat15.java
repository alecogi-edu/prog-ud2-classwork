package com.prog;

import java.util.Scanner;

/**
 * Fes un programa que demane a l'usuari un número enter de 3 xifres i mostre la seua descomposició en centenes,
 * desenes i unitats. Si el número introduït és 258, aleshores mostrarà: 258 = 2^100 + 5^10+8
 */
public class Activitat15 {

    public static void main(String[] args) {

        Scanner scannerKey = new Scanner(System.in);

        System.out.println("Introdueix un enter de 3 xifres");
        int numberToSplit = scannerKey.nextInt();

        int hundred = numberToSplit / 100;
        int ten = (numberToSplit % 100) / 10;
        int units = numberToSplit % 10;
        System.out.printf("%d = %d^100 + %d^10 + %d", numberToSplit, hundred, ten, units);

    }
}
