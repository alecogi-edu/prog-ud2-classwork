package com.prog;

import java.util.Scanner;

/**
 * Volem resoldre expressions del tipus ax+b=c, és a dir, equacions de 1r grau, on x és l'incògnita,
 * a i b són els coeficients i c és el resultat. Implementa un programa on demanes a l'usuari els valors dels coeficients
 * i el resultat, i que ens retorne el valor d'x.
 */
public class Activitat14 {

    public static void main(String[] args) {

        Scanner scannerKey = new Scanner(System.in);
        System.out.println("Coeficiente A");
        float coefficientA = scannerKey.nextFloat();

        System.out.println("Coeficiente B");
        float coefficientB = scannerKey.nextFloat();

        System.out.println("Resultado de la ecuación");
        float result = scannerKey.nextFloat();

        float xValue = (result-coefficientB)/coefficientA;
        System.out.printf("En la ecuación %.2fx + %.2f = %.2f, x=%.2f ", coefficientA, coefficientB, result, xValue);

    }
}
