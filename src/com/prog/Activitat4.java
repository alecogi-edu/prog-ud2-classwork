package com.prog;

/**
 * Escriu un programa en el qual es declaren les variables senceres x e i. Asignales els valors 144 i 999 respectivament.
 * A continuación, mostra per pantalla el valor de cada variable, la suma, la resta, la divisió i la multiplicació.
 */
public class Activitat4 {

   public static void main(String[] args) {

        int x = 144;
        int i = 999;

        int plusResult = x + i;
        System.out.println("La suma és: " + plusResult);

        int subResult = x - i;
        System.out.println("La resta és: " + subResult);

        float divResult = x / (float) i;
        System.out.println("La divisió és: " + divResult);

        int multResult = x * i ;
        System.out.println("La multiplicació és: " + multResult);

    }
}
