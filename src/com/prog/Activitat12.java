package com.prog;

import java.util.Scanner;

public class Activitat12 {

    /**
     * Escriu un programa que sol·licite a l'usuari una quantitat de segons i la convertisca en dies, hores, minuts i segons.
     * Visualitza el resultat per pantalla.
     */
    public static void main(String[] args) {

        Scanner scannerKey = new Scanner(System.in);

        System.out.println("Introduce segundos a convertir");
        int seconds = scannerKey.nextInt();

        int days = seconds / 86400;
        seconds -= days * 86400;
        int hours = seconds / 3600;
        seconds -= hours * 3600;
        int minutes = seconds / 60;
        seconds -= minutes * 60;

        System.out.printf("%d dias, %d horas, %d minutos, %d segundos \n", days, hours, minutes, seconds);

    }
}
