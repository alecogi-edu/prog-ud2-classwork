package com.prog;

import java.util.Scanner;

/**
 * Implementa un programa que demane a l'usuari dos números, els guarde en dos variables i intercanvie els valors.
 * Ha de mostrar els valors intercanviats.
 */
public class Activitat13 {

    public static void main(String[] args) {

        Scanner scannerKey = new Scanner(System.in);

        System.out.println("Introduce el número 1");
        int num1 = scannerKey.nextInt();

        System.out.println("Introduce el número 2");
        int num2 = scannerKey.nextInt();

        int aux = num1;
        num1 = num2;
        num2 = aux;

        System.out.println("Tras realizar el cambio");
        System.out.println("El número 1 es: " + num1);
        System.out.println("El número 2 es: " + num2);

    }
}
