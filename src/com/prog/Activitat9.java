package com.prog;

import java.util.Scanner;

/*
 * Escriu un programa per a calcular i mostrar l'àrea (A =2PIr2 + 2PIrh) i el volum (V=PIr 2·h) d'un cilindre.
 * Els valors del diàmetre i l'alçada se li demanaran a l'usuari.
 */
public class Activitat9 {

    public static void main(String[] args) {

        Scanner scannerKey = new Scanner(System.in);

        System.out.println("Introduce el diámetro");
        int diameter = scannerKey.nextInt();
        float radius = diameter / 2f;

        System.out.println("Introduce la altura");
        float height = scannerKey.nextFloat();

        double area = (2 * Math.PI * Math.pow(radius, 2)) + (2 * Math.PI * radius * height);
        double volume = Math.PI * Math.pow(radius, 2) * height ;

        System.out.printf("El area es %6.2f \n", area);
        System.out.printf("El volumen es %6.2f \n", volume);

    }
}
