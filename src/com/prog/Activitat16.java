package com.prog;

import java.util.Scanner;

/**
 * Dissenya un programa que demane a l'usuari les coordenades dels 2 punts
 * i calcule la distància que n'hi ha entre ells.
 */
public class Activitat16 {

    public static void main(String[] args) {

        Scanner scannerKey = new Scanner(System.in);

        System.out.println("Dades del punt A");
        System.out.print("x1: ");
        int puntACoordX = scannerKey.nextInt();
        System.out.print("y1: ");
        int puntACoordY  = scannerKey.nextInt();

        System.out.println("Dades del punt B");
        System.out.print("x2: ");
        int puntBCoordX = scannerKey.nextInt();
        System.out.print("y2: ");
        int puntBCoordY  = scannerKey.nextInt();

        double distance = Math.pow((puntBCoordX-puntACoordX), 2f) + Math.pow((puntBCoordY-puntACoordY), 2f);
        distance = Math.sqrt(distance);

        System.out.printf("La distancia entre (%d,%d) y (%d,%d) es %f \n", puntACoordX,
                puntACoordY, puntBCoordX, puntBCoordY, distance);

    }
}
