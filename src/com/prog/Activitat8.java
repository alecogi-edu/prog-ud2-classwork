package com.prog;

/**
 * Escriu un programa que calcule el preu final d'un producte segons la seua base imposable
 * (preu abans d'impostos) Tenint en compte...
 */
public class Activitat8 {

    public static void main(String[] args) {

        float precioProducto = 5000;

        final float IVA_GENERAL = 0.21f;
        final float IVA_REDUIT = 0.21f;
        final float IVA_SUPER_REDUIT = 0.21f;

        final float PROMO_HALF = 0.5f;
        final int PROMO_EURO = 5;
        final float PROMO_PERCENT = 0.05f;

        System.out.println("Base Imponible : " + precioProducto + " €");

        // Precios del producto sin promoción
        System.out.println("\nPrecios del producto sin promoción: ");
        System.out.println("----------------------------------- ");
        float resultado = precioProducto + (precioProducto * IVA_GENERAL);
        System.out.println("Precio con IVA : " + resultado + " €");
        resultado = precioProducto + (precioProducto * IVA_REDUIT);
        System.out.println("Precio con IVA reducido : " + resultado + " €");
        resultado = precioProducto + (precioProducto * IVA_SUPER_REDUIT);
        System.out.println("Precio con IVA reducido : " + resultado + " €");

        // Precios del producto promoción a mitad de precio
        System.out.println("\nPrecios del producto con promoción mitad de precio: ");
        System.out.println("----------------------------------- ");
        resultado = (precioProducto + (precioProducto * IVA_GENERAL)) * PROMO_HALF;
        System.out.println("Precio con IVA : " + resultado + " €");

        resultado = (precioProducto + (precioProducto * IVA_REDUIT)) * PROMO_HALF;
        System.out.println("Precio con IVA reducido : " + resultado + " €");

        resultado = (precioProducto + (precioProducto * IVA_SUPER_REDUIT)) * PROMO_HALF;
        System.out.println("Precio con IVA reducido : " + resultado + " €");

        // Precios del producto con promoción -5€
        System.out.println("\nPrecios del producto con promoción - 5€ ");
        System.out.println("----------------------------------- ");

        resultado = precioProducto + (precioProducto * IVA_GENERAL);
        resultado -= PROMO_EURO;
        System.out.println("Precio con IVA : " + resultado + " €");

        resultado = precioProducto + (precioProducto * IVA_REDUIT);
        resultado -= PROMO_EURO;
        System.out.println("Precio con IVA reducido : " + resultado + " €");

        resultado = precioProducto + (precioProducto * IVA_SUPER_REDUIT);
        resultado -= PROMO_EURO;
        System.out.println("Precio con IVA reducido : " + resultado + " €");

        // Precios del producto con promoción - 5%
        System.out.println("\nPrecios del producto con promoción - 5% ");
        System.out.println("----------------------------------- ");

        resultado = precioProducto + (precioProducto * IVA_GENERAL);
        resultado -= resultado * PROMO_PERCENT;
        System.out.println("Precio con IVA : " + resultado + " €");

        resultado = precioProducto + (precioProducto * IVA_REDUIT);
        resultado -= resultado * PROMO_PERCENT;
        System.out.println("Precio con IVA reducido : " + resultado + " €");

        resultado = precioProducto + (precioProducto * IVA_SUPER_REDUIT);
        resultado -= resultado * PROMO_PERCENT;
        System.out.println("Precio con IVA reducido : " + resultado + " €");

    }
}
