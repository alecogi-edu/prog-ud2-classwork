package com.prog;

import java.util.Scanner;

/*
 * Escriu un programa que demane a l'usuari els valors dels costats d'un rectangle (ample i alt). A partir d'estes dades,
 * haurà de calcular i escriure en pantalla les longituds, el perímetre i l'àrea del rectangle.
 */
public class Activitat10 {

    public static void main(String[] args) {

        Scanner scannerKey = new Scanner(System.in);

        System.out.println("Introduce el ancho");
        float width = scannerKey.nextFloat();

        System.out.println("Introduce el alto");
        float height = scannerKey.nextFloat();

        System.out.println("El ancho és :" + width);
        System.out.println("El alto és :" + height);
        System.out.println("El perímetre és :" + (width*2 + height*2));
        System.out.println("El àrea es :" + (width*height));

    }
}
