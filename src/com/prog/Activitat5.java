package com.prog;

/**
 * Implementa un programa que mostre quant valdran unes deportives amb un preu de 85.00 euros,
 * si estan rebaixades un 15%.
 */
public class Activitat5 {

    public static void main(String[] args) {

        float prize = 85f;
        float totalDiscount = prize * 0.15f;
        System.out.println("Precio rebajado :" + ( prize - totalDiscount ));

    }
}
